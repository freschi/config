package at.femoweb.config;

import java.util.HashMap;

/**
 * Created by felix on 1/20/15.
 */
public final class Config {

    private static HashMap<String, Catalog> catalogs;

    static {
        catalogs = new HashMap<>();
    }

    /**
     * Opens a catalog but does not build it from xml
     *
     * @param name the name of the catalog
     * @return the requested catalog
     */
    public static Catalog openCatalog(String name) {
        return openCatalog(name, false);
    }

    /**
     * Opens a catalog and builds it if requested.
     *
     * This method should be preferred over Catalog.open() because it caches already opened Catalogs and returns them if found
     *
     * @param name the name of the catalog
     * @param buildXml <strong>true - </strong>the catalog will build itself from xml, <strong>false - </strong>the catalog will only load itself
     * @return the requested dialog
     */
    public static Catalog openCatalog(String name, boolean buildXml) {
        if(catalogs.containsKey(name)) {
            return catalogs.get(name);
        } else {
            Catalog catalog = Catalog.open(name, buildXml);
            catalogs.put(name, catalog);
            return catalog;
        }
    }
}
