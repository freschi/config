package at.femoweb.config.entries;

import at.femoweb.html.Tag;

import java.util.ArrayList;

/**
 * Created by felix on 1/18/15.
 */
public class List extends AbstractCollectionEntry<Integer> {
    
    private ArrayList<ConfEntry> entries;
    
    public List () {
        this.entries = new ArrayList<>();
    }
    
    @Override
    protected ConfEntry getEntry(Integer integer) {
        if(integer < entries.size()) {
            return entries.get(integer);
        } else {
            throw new KeyNotFoundException("Index out of bounds " + integer);
        }
    }

    @Override
    protected void setEntry(Integer integer, ConfEntry entry) {
        entries.set(integer, entry);
    }

    @Override
    protected Integer addEntry(ConfEntry entry) {
        entries.add(entry);
        return entries.indexOf(entry);
    }

    @Override
    public boolean has(Integer integer) {
        return entries.size() > integer;
    }

    @Override
    public int count() {
        return entries.size();
    }

    @Override
    public Type getType() {
        return Type.LIST;
    }

    @Override
    public String export() {
        String result = "@[";
        for (int i = 0; i < entries.size(); i++) {
            result += entries.get(i).export();
            if(i != entries.size() - 1) {
                result += ",";
            }
        }
        return result + "]";
    }

    @Override
    public Tag exportXml() {
        Tag xml = new Tag();
        xml.attribute("type", "list");
        for(ConfEntry entry : entries) {
            Tag export = entry.exportXml();
            export.name("item");
            xml.add(export);
        }
        return xml;
    }
}
