package at.femoweb.config.entries;

/**
 * Created by felix on 1/16/15.
 */
public abstract class AbstractCollectionEntry<KEY> implements ConfEntry {
    
    protected abstract ConfEntry getEntry(KEY key);
    protected abstract void setEntry(KEY key, ConfEntry entry);
    protected abstract KEY addEntry(ConfEntry entry);
    public abstract boolean has(KEY key);
    public abstract int count();

    public int getInt(KEY key) {
        ConfEntry entry = getEntry(key);
        if(entry.getType() == Type.NUMBER && entry instanceof SimpleValueEntry) {
            return ((SimpleValueEntry) entry).asInt();
        } else {
            throw new ClassCastException("Entry of type " + entry.getType() + " cannot be casted to int");
        }
    }

    public byte getByte(KEY key) {
        ConfEntry entry = getEntry(key);
        if(entry.getType() == Type.NUMBER && entry instanceof SimpleValueEntry) {
            return ((SimpleValueEntry) entry).asByte();
        } else {
            throw new ClassCastException("Entry of type " + entry.getType() + " cannot be casted to byte");
        }
    }

    public char getChar(KEY key) {
        ConfEntry entry = getEntry(key);
        if(entry.getType() == Type.NUMBER && entry instanceof SimpleValueEntry) {
            return ((SimpleValueEntry) entry).asChar();
        } else {
            throw new ClassCastException("Entry of type " + entry.getType() + " cannot be casted to char");
        }
    }

    public short getShort(KEY key) {
        ConfEntry entry = getEntry(key);
        if(entry.getType() == Type.NUMBER && entry instanceof SimpleValueEntry) {
            return ((SimpleValueEntry) entry).asShort();
        } else {
            throw new ClassCastException("Entry of type " + entry.getType() + " cannot be casted to short");
        }
    }

    public long getLong(KEY key) {
        ConfEntry entry = getEntry(key);
        if(entry.getType() == Type.NUMBER && entry instanceof SimpleValueEntry) {
            return ((SimpleValueEntry) entry).asLong();
        } else {
            throw new ClassCastException("Entry of type " + entry.getType() + " cannot be casted to long");
        }
    }
    
    public boolean getBoolean(KEY key) {
        ConfEntry entry = getEntry(key);
        if(entry.getType() == Type.BOOLEAN && entry instanceof  SimpleValueEntry) {
            return ((SimpleValueEntry) entry).asBoolean();
        } else {
            throw new ClassCastException("Entry of type " + entry.getType() + " cannot be casted to boolean");
        }
    }
    
    public String getString(KEY key) {
        ConfEntry entry = getEntry(key);
        if(entry instanceof SimpleValueEntry) {
            return ((SimpleValueEntry) entry).asString();
        } else {
            throw new ClassCastException("Entry of type " + entry.getType() + " cannot be casted to string");
        }
    }
    
    public float getFloat(KEY key) {
        ConfEntry entry = getEntry(key);
        if(entry.getType() == Type.DECIMAL && entry instanceof SimpleValueEntry) {
            return ((SimpleValueEntry) entry).asFloat();
        } else {
            throw new ClassCastException("Entry of type " + entry.getType() + " cannot be casted to float");
        }
    }

    public double getDouble(KEY key) {
        ConfEntry entry = getEntry(key);
        if(entry.getType() == Type.DECIMAL && entry instanceof SimpleValueEntry) {
            return ((SimpleValueEntry) entry).asDouble();
        } else {
            throw new ClassCastException("Entry of type " + entry.getType() + " cannot be casted to double");
        }
    }
    
    public Group getGroup(KEY key) {
        if(has(key)) {
            ConfEntry entry = getEntry(key);
            if(entry.getType() == Type.GROUP && entry instanceof Group) {
                return (Group) entry;
            } else {
                throw new ClassCastException("Entry of type " + entry.getType() + " cannot be casted to group");
            }
        } else {
            Group group = new Group();
            setEntry(key, group);
            return group;
        }
    }
    
    public List getList(KEY key) {
        if(has(key)) {
            ConfEntry entry = getEntry(key);
            if(entry.getType() == Type.LIST && entry instanceof  List) {
                return (List) entry;
            } else {
                throw new ClassCastException("Entry of type " + entry.getType() + " cannot be casted to list");
            }
        } else {
            List list = new List();
            setEntry(key, list);
            return list;
        }
    }
    
    public void setInt(KEY key, int value) {
        setEntry(key, new SimpleValueEntry<>(value));
    }
    public void setByte(KEY key, byte value) {
        setEntry(key, new SimpleValueEntry<>(value));
    }
    public void setShort(KEY key, short value) {
        setEntry(key, new SimpleValueEntry<>(value));
    }
    public void setChar(KEY key, char value) {
        setEntry(key, new SimpleValueEntry<>(value));
    }
    public void setLong(KEY key, long value) {
        setEntry(key, new SimpleValueEntry<>(value));
    }
    public void setBoolean(KEY key, boolean value) {
        setEntry(key, new SimpleValueEntry<>(value));
    }
    public void setString(KEY key, String value) {
        setEntry(key, new SimpleValueEntry<>(value));
    }
    public void setDouble(KEY key, double value) {
        setEntry(key, new SimpleValueEntry<>(value));
    }
    public void setFloat(KEY key, float value) {
        setEntry(key, new SimpleValueEntry<>(value));
    }
    public void setGroup(KEY key, Group value) {
        setEntry(key, value);
    }
    public void setList(KEY key, List value) {
        setEntry(key, value);
    }
    
    public void addInt(int value) {
        addEntry(new SimpleValueEntry<>(value));
    }
    public void addByte(byte value) {
        addEntry(new SimpleValueEntry<>(value));
    }
    public void addShort(short value) {
        addEntry(new SimpleValueEntry<>(value));
    }
    public void addChar(char value) {
        addEntry(new SimpleValueEntry<>(value));
    }
    public void addLong(long value) {
        addEntry(new SimpleValueEntry<>(value));
    }
    public void addBoolean(boolean value) {
        addEntry(new SimpleValueEntry<>(value));
    }
    public void addString(String value) {
        addEntry(new SimpleValueEntry<>(value));
    }
    public void addDouble(double value) {
        addEntry(new SimpleValueEntry<>(value));
    }
    public void addFloat(float value) {
        addEntry(new SimpleValueEntry<>(value));
    }
    
    public Group addGroup() {
        Group group = new Group();
        addEntry(group);
        return group;
    }
    
    public List addList() {
        List list = new List();
        addEntry(list);
        return list;
    }
    
}
