package at.femoweb.config.entries;

import at.femoweb.html.Tag;

/**
 * Created by felix on 1/16/15.
 */
public interface ConfEntry {
    
    public enum Type {
        GROUP, LIST, NUMBER, DECIMAL, BOOLEAN, NULL, STRING
    }
    
    public abstract Type getType();
    
    public abstract String export();
    public abstract Tag exportXml();
}
