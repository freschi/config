package at.femoweb.config.entries;

/**
 * Created by felix on 1/16/15.
 */
public class KeyNotFoundException extends RuntimeException {

    public KeyNotFoundException(String s) {
        super(s);
    }
}
