package at.femoweb.config.entries;

import at.femoweb.html.Tag;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 * Created by felix on 1/16/15.
 */
public class SimpleValueEntry<KEY> implements ConfEntry{
    
    private KEY value;
    private Class<KEY> type;
    private Type entryType;
    
    public SimpleValueEntry(KEY value) {
        if (value == null) {
            entryType = Type.NULL;
        } else {
            type = (Class<KEY>) value.getClass();
            this.value = value;
            if (type == int.class || type == Integer.class || type == byte.class || type == Byte.class || type == short.class || type == Short.class || type == long.class || type == Long.class || type == char.class || type == Character.class) {
                entryType = Type.NUMBER;
            } else if (type == boolean.class || type == Boolean.class) {
                entryType = Type.BOOLEAN;
            } else if (type == String.class) {
                entryType = Type.STRING;
                this.value = (KEY) StringEscapeUtils.escapeJava(String.class.cast(value));
            } else if (type == float.class || type == Float.class || type == double.class || type == Double.class) {
                entryType = Type.DECIMAL;
            }
        }
    }
    
    public int asInt() {
        if(entryType == Type.NUMBER) {
            return (int) getLongValue();
        } else {
            throw new ClassCastException(type.getName() + " cannot be casted to int");
        }
    }
    
    public byte asByte() {
        if(entryType == Type.NUMBER) {
            return (byte) getLongValue();
        } else {
            throw new ClassCastException(type.getName() + " cannot be casted to byte");
        }
    }

    public short asShort() {
        if(entryType == Type.NUMBER) {
            return (short) getLongValue();
        } else {
            throw new ClassCastException(type.getName() + " cannot be casted to short");
        }
    }

    public long asLong() {
        if(entryType == Type.NUMBER) {
            return getLongValue();
        } else {
            throw new ClassCastException(type.getName() + " cannot be casted to long");
        }
    }

    public char asChar() {
        if(entryType == Type.NUMBER) {
            return (char) getLongValue();
        } else {
            throw new ClassCastException(type.getName() + " cannot be casted to char");
        }
    }
    
    public boolean asBoolean() {
        if(entryType == Type.NULL)
            return false;
        if(entryType == Type.BOOLEAN) {
            return Boolean.class.cast(value);
        } else {
            throw new ClassCastException(type.getName() + " cannot be casted to boolean");
        }
    }
    
    public float asFloat() {
        if(entryType == Type.NULL)
            return 0;
        if(entryType == Type.NUMBER) {
            return (float) getLongValue();
        } else if (entryType == Type.DECIMAL) {
            if(type == float.class || type == Float.class) {
                return Float.class.cast(value).floatValue();
            } else {
                return Double.class.cast(value).floatValue();
            }
        } else {
            throw new ClassCastException(type.getName() + " cannot be casted to float");
        }
    }

    public double asDouble() {
        if(entryType == Type.NULL)
            return 0;
        if(entryType == Type.NUMBER) {
            return (double) getLongValue();
        } else if (entryType == Type.DECIMAL) {
            if(type == float.class || type == Float.class) {
                return Float.class.cast(value).doubleValue();
            } else {
                return Double.class.cast(value).doubleValue();
            }
        } else {
            throw new ClassCastException(type.getName() + " cannot be casted to double");
        }
    }
    
    public String asString() {
        if(entryType == Type.NULL)
            return null;
        if(entryType == Type.STRING) {
             return StringEscapeUtils.unescapeJava(String.class.cast(value));
        }
        return value.toString();
    }
    
    private long getLongValue() {
        if(entryType == Type.NULL) {
            return 0;
        }
        if(entryType != Type.NUMBER) {
            throw new ClassCastException(type.getName() + " cannot be converted to long");
        }
        if(type == Integer.class || type == int.class) {
            return Integer.class.cast(value);
        } else if (type == Short.class || type == short.class) {
            return Short.class.cast(value);
        } else if (type == Byte.class || type == byte.class) {
            return Byte.class.cast(value);
        } else if (type == Character.class || type == char.class) {
            return Character.class.cast(value);
        } else if (type == Long.class || type == long.class) {
            return Long.class.cast(value);
        } else {
            throw new ClassCastException(type.getName() + " cannot be converted to long");
        }
    }

    @Override
    public Type getType() {
        return entryType;
    }

    @Override
    public String export() {
        return value.toString();
    }

    @Override
    public Tag exportXml() {
        Tag xml = new Tag();
        xml.add(export());
        return xml;
    }
}
