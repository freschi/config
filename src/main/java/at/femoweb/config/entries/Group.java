package at.femoweb.config.entries;

import at.femoweb.html.Tag;

import java.util.HashMap;

/**
 * Created by felix on 1/16/15.
 */
public class Group extends AbstractCollectionEntry<String> {
    
    private HashMap<String, ConfEntry> entries;
    
    public Group () {
        entries = new HashMap<>();
    }
    
    @Override
    protected ConfEntry getEntry(String s) {
        if(entries.containsKey(s)) {
            return entries.get(s);
        } else {
            throw new KeyNotFoundException("Key not in key list " + s);
        }
    }

    @Override
    protected void setEntry(String s, ConfEntry entry) {
        entries.put(s, entry);
    }

    @Override
    protected String addEntry(ConfEntry entry) {
        throw new UnsupportedOperationException("Group can't even handle things without keys");
    }

    @Override
    public boolean has(String s) {
        return entries.containsKey(s);
    }

    @Override
    public int count() {
        return entries.size();
    }

    @Override
    public Type getType() {
        return Type.GROUP;
    }

    @Override
    public String export() {
        String result = "$[";
        String[] keys = new String[entries.size()];
        keys = entries.keySet().toArray(keys);
        for (int i = 0; i < keys.length; i++) {
            String key = keys[i];
            result += key + "=" + entries.get(key).export();
            if(i != keys.length - 1) {
                result += ",";
            }
        }
        return result + "]";
    }

    @Override
    public Tag exportXml() {
        Tag xml = new Tag();
        for(String key : entries.keySet()) {
            Tag export = entries.get(key).exportXml();
            export.name(key);
            xml.add(export);
        }
        return xml;
    }
}
