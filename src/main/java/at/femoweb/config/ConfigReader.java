package at.femoweb.config;

import at.femoweb.config.entries.Group;
import at.femoweb.config.entries.List;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

/**
 * Created by felix on 1/19/15.
 */
public final class ConfigReader {

    public static final Logger log = LogManager.getLogger("Config");

    public static final String readGroup(String line, Group group) {
        String name;
        name = line.substring(0, line.indexOf("="));
        String rest = line.substring(line.indexOf("=") + 1);
        rest = StringEscapeUtils.unescapeJava(rest);
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(rest.getBytes("UTF-8"));
            byteArrayInputStream.skip(1);
            readGroup(group, byteArrayInputStream);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return name;
    }

    private static final void readGroup(Group group, ByteArrayInputStream byteArrayInputStream) {
        String key = "", value = "";
        byteArrayInputStream.skip(1);
        int read;
        boolean escape = false;
        boolean inKey = true;
        while((read = byteArrayInputStream.read()) > 0) {
            if(read == '\\') {
                escape = true;
            }
            if(!escape && (read == ',' || read == ']')) {
                inKey = true;
                Object val = parseObject(value);
                if(val.getClass() == long.class || val.getClass() == Long.class) {
                    group.setLong(key, (long) val);
                } else if (val.getClass() == double.class || val.getClass() == Double.class) {
                    group.setDouble(key, (double) val);
                } else if (val.getClass() == boolean.class || val.getClass() == Boolean.class) {
                    group.setBoolean(key, (boolean) val);
                } else {
                    group.setString(key, StringEscapeUtils.unescapeJava(value));
                }
                value = "";
                key = "";
                if(read == ']') {
                    break;
                }
            } else if (!escape && read == '=') {
                inKey = false;
            } else {
                if(inKey) {
                    key += (char) read;
                } else {
                    if(!escape && read == '@') {
                        List list = new List();
                        readList(list, byteArrayInputStream);
                        group.setList(key, list);
                        byteArrayInputStream.skip(1);
                    } else if (!escape && read == '$') {
                        Group sub = new Group();
                        readGroup(sub, byteArrayInputStream);
                        group.setGroup(key, sub);
                        byteArrayInputStream.skip(1);
                    } else {
                        value += (char) read;
                    }
                }
            }
            if(read != '\\') {
                escape = false;
            }
        }
    }

    private static final void readList(List list, ByteArrayInputStream byteArrayInputStream) {
        byteArrayInputStream.skip(1);
        String value = "";
        int read;
        boolean escape = false;
        while((read = byteArrayInputStream.read()) >= 0) {
            if(!escape && (read == ']' || read == ',')) {
                Object val = parseObject(value);
                if(val.getClass() == long.class) {
                    list.addLong((long) val);
                } else if (val.getClass() == double.class) {
                    list.addDouble((double) val);
                } else if (val.getClass() == boolean.class) {
                    list.addBoolean((boolean) val);
                } else {
                    list.addString(StringEscapeUtils.unescapeJava(value));
                }
                value = "";
                if(read == ']') {
                    break;
                }
            } else {
                if(!escape && read == '@') {
                    List sub = list.addList();
                    readList(sub, byteArrayInputStream);
                    byteArrayInputStream.skip(1);
                } else if (!escape && read == '$') {
                    Group sub = list.addGroup();
                    readGroup(sub, byteArrayInputStream);
                    byteArrayInputStream.skip(1);
                } else {
                    value += (char) read;
                }
            }
        }
    }

    private static final Object parseObject(String value) {
        if(isNumber(value)) {
            return Long.parseLong(value);
        } else if (isDecimal(value)) {
            return Double.parseDouble(value);
        } else if (value.toString().equals("true") || value.toString().equals("false")) {
            return Boolean.parseBoolean(value);
        } else {
            return value.toString();
        }
    }

    private static boolean isNumber(String value) {
        try {
            Long.parseLong(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean isDecimal(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
