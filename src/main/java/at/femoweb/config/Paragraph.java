package at.femoweb.config;

import at.femoweb.config.entries.ConfEntry;
import at.femoweb.config.entries.Group;

import java.util.HashMap;

/**
 * Created by felix on 1/16/15.
 */
public class Paragraph {
    
    private Group entry;
    
    private String name;
    private Article article;
    
    public static final Paragraph open(Article article, String name) {
        return new Paragraph(article, name);
    }

    protected static Paragraph construct(Article article, String name, Group entry) {
        Paragraph paragraph = new Paragraph(article, name);
        paragraph.entry = entry;
        return paragraph;
    }

    private Paragraph(Article article, String name) {
        this.entry = new Group();
        this.name = name;
        this.article = article;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Article getArticle() {
        return article;
    }
    
    public Group openEntry() {
        return entry;
    }
}
