package at.femoweb.config;

import at.femoweb.config.entries.Group;
import at.femoweb.config.entries.List;
import at.femoweb.html.HTMLReader;
import at.femoweb.html.Tag;
import at.femoweb.html.TextTag;

import java.io.*;
import java.util.HashMap;

/**
 * Created by felix on 1/16/15.
 */
public class Article {
    
    private HashMap<String, Paragraph> paragraphs;
    private long built;

    private String name;
    private Catalog catalog;
    private boolean build;

    /**
     * Creates a new article with the given catalog and name.
     *
     * If you have an instance of Catalog, please use Catalog.openArticle(String) to create a new article
     *
     * @see at.femoweb.config.Catalog
     *
     * @param catalog the parent catalog of this article
     * @param name the name of the article
     * @return the requested paragraph
     */
    public static final Article open(Catalog catalog, String name) {
        return new Article(catalog, name);
    }
    
    private Article (Catalog catalog, String name) {
        this.name = name;
        this.catalog = catalog;
        this.paragraphs = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Catalog getCatalog() {
        return catalog;
    }
    
    public Paragraph openParagraph(String name) {
        if(paragraphs.containsKey(name)) {
            return paragraphs.get(name);
        } else {
            Paragraph paragraph = Paragraph.open(this, name);
            paragraphs.put(name, paragraph);
            return paragraph;
        }
    }
    
    protected Group getMeta() {
        Group meta = new Group();
        meta.setString("name", name);
        meta.setBoolean("build", build);
        meta.setString("path", "config/" + catalog.getName() + "/" + (name.endsWith(".conf") ? name : name + ".conf"));
        return meta;
    }
    
    protected void export(File article) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(article);
        PrintStream printStream = new PrintStream(outputStream);
        for(String key : paragraphs.keySet()) {
            printStream.println(key + "=" + paragraphs.get(key).openEntry().export());
        }
        outputStream.close();
    }

    protected void exportXml(File xml, long built) throws FileNotFoundException {
        Tag config = new Tag("config");
        config.attribute("built", built + "");
        config.attribute("name", name);
        for(String key : paragraphs.keySet()) {
            Group group = paragraphs.get(key).openEntry();
            Tag paragraph = group.exportXml();
            paragraph.name(key);
            config.add(paragraph);
        }
        PrintStream out = new PrintStream(new FileOutputStream(xml));
        out.print(config.render());
        out.close();
    }

    protected static Article importXml(File xml, Catalog catalog) throws IOException {
        Tag config = HTMLReader.read(xml).get(0);
        if(config.name().equals("config")) {
            Article article = new Article(catalog, config.attribute("name"));
            if(config.attribute("built") != null)
                article.built = Long.parseLong(config.attribute("built"));
            for(Tag child : config.children()) {
                Paragraph paragraph = article.openParagraph(child.name());
                Group conf = paragraph.openEntry();
                readGroup(conf, child);
            }
            return article;
        } else {
            throw new UnsupportedOperationException("Invalid xml configuration file");
        }
    }

    protected static Article build(File conf, String name, Catalog catalog) {
        Article article = new Article(catalog, name);
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(conf)));
            String line;
            while((line = reader.readLine()) != null) {
                Group group = new Group();
                String pname = ConfigReader.readGroup(line, group);
                article.paragraphs.put(pname, Paragraph.construct(article, pname, group));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return article;
    }

    private static void readGroup(Group conf, Tag xml) {
        for(Tag child : xml.children()) {
            if(child.children().size() == 1 && child.get(0) instanceof TextTag) {
                setValue(child.name(), child.get(0).text(), conf);
            } else {
                if(child.attribute("type") != null && child.attribute("type").equals("list")) {
                    List list = new List();
                    conf.setList(child.name(), list);
                    readList(list, child);
                } else {
                    Group group = new Group();
                    conf.setGroup(child.name(), group);
                    readGroup(group, child);
                }
            }
        }
    }

    private static void readList(List list, Tag xml) {
        for(Tag child : xml.children()) {
            if(child.children().size() == 1 && child.get(0) instanceof TextTag) {
                addValue(child.get(0).text(), list);
            } else {
                if(child.attribute("type") != null && child.attribute("type").equals("list")) {
                    List conf = list.addList();
                    readList(conf, child);
                } else {
                    Group group = list.addGroup();
                    readGroup(group, child);
                }
            }
        }
    }

    private static void setValue(String key, String value, Group group) {
        if(isNumber(value)) {
            group.setLong(key, Long.parseLong(value));
        } else if (isDecimal(value)) {
            group.setDouble(key, Double.parseDouble(value));
        } else if (value.toString().equals("true") || value.toString().equals("false")) {
            group.setBoolean(key, Boolean.parseBoolean(value));
        } else {
            group.setString(key, value);
        }
    }

    private static void addValue(String value, List list) {
        if(isNumber(value)) {
            list.addLong(Long.parseLong(value));
        } else if (isDecimal(value)) {
            list.addDouble(Double.parseDouble(value));
        } else if (value.toString().equals("true") || value.toString().equals("false")) {
            list.addBoolean(Boolean.parseBoolean(value));
        } else {
            list.addString(value);
        }
    }

    private static boolean isNumber(String value) {
        try {
            Long.parseLong(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean isDecimal(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean isBoolean(String value) {
        try {
            Boolean.parseBoolean(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public long getBuilt() {
        return built;
    }

    public void setBuilt(long built) {
        this.built = built;
    }

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    public boolean isBuild() {
        return build;
    }

    public void setBuild(boolean build) {
        this.build = build;
    }
}
