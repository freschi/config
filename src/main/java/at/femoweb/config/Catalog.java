package at.femoweb.config;

import at.femoweb.config.entries.Group;
import at.femoweb.html.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.print.Printable;
import java.io.*;
import java.util.HashMap;

/**
 * Created by felix on 1/16/15.
 */
public class Catalog {
    
    private HashMap<String, Article> articles;
    
    public static final Logger log = LogManager.getLogger("Config");
    
    private String name;
    private boolean exportXml = true;
    private boolean exportStartup = true;
    private boolean exportShutdown = true;
    private boolean buildXml = false;
    private long built;
    
    private File catalogRoot;
    private File xmlRoot;

    /**
     * Constructs a new catalog and loads it from files if possible
     *
     * Please use Config.openCatalog(String) if you plan to open the same catalog more than once
     *
     * @see at.femoweb.config.Config
     * @param name the name of the catalog
     * @return the requested catalog
     */
    public static final Catalog open(String name) {
        Catalog catalog = new Catalog(name);
        catalog.checkRoots();
        return catalog;
    }

    /**
     * Constructs a new catalog and loads it from files or xml if possible
     *
     * Please use Config.openCatalog(String, boolean) if you plan to open the same catalog more than once
     *
     * @see at.femoweb.config.Config
     * @param name the name of the catalog
     * @param buildXml <strong>true - </strong>the catalog will build itself from xml, <strong>false - </strong>the catalog will only load itself
     * @return the requested catalog
     */
    public static final Catalog open(String name, boolean buildXml) {
        Catalog catalog = new Catalog(name, buildXml);
        catalog.checkRoots();
        return catalog;
    }

    private Catalog(String name) {
        this.name = name;
        this.articles = new HashMap<String, Article>();
        catalogRoot = new File("config/" + name + ".cat");
        xmlRoot = new File("config/" + name);
    }

    private Catalog(String name, boolean buildXml) {
        this(name);
        this.buildXml = buildXml;
        if(buildXml) {
            buildXml();
        } else {
            build();
        }
    }
    
    private final void checkRoots () {
        if(!catalogRoot.exists()) {
            if(!catalogRoot.mkdirs()) {
                log.warn("Could not generate catalog directory. Saving will not be possible");
            }
        } else if (!catalogRoot.isDirectory()) {
            log.warn("Catalog directory is a file...");
        }
        if(!xmlRoot.exists()) {
            if(!xmlRoot.mkdirs()) {
                log.warn("Could not generate xml directory. Exporting will not be possible");
            }
        } else if (!xmlRoot.isDirectory()) {
            log.warn("Xml directory is a file...");
        }
    }

    /**
     * Opens or creates a requested Article.
     *
     * If the article cannot be found, the catalog will create a new one
     *
     * @param name the name of the article
     * @return the requested article
     */
    public Article openArticle(String name) {
        synchronized (articles) {
            if (articles.containsKey(name)) {
                return articles.get(name);
            } else {
                Article article = Article.open(this, name);
                articles.put(name, article);
                return article;
            }
        }
    }

    /**
     * Automatically writes the catalog to the suitable folders and files.
     * @throws IOException usual file related stuff...
     * @throws IOException usual file related stuff...
     */
    public final void export() throws IOException {
        synchronized (articles) {
            OutputStream mapOut = new FileOutputStream(new File(catalogRoot, "catalog.map"));
            log.debug("Writing catalog map file");
            long built = writeMap(new PrintStream(mapOut));
            log.debug("Writing articles");
            String[] keys = new String[articles.size()];
            keys = articles.keySet().toArray(keys);
            for (int i = 0; i < keys.length; i++) {
                File art = new File(catalogRoot, String.format("%02x", i) + ".art");
                Article article = articles.get(keys[i]);
                Group meta = article.getMeta();
                article.export(art);
                if(exportXml) {
                    File xml = new File(meta.getString("path"));
                    article.exportXml(xml, built);
                }
            }
        }
    }
    
    private long writeMap(PrintStream out) {
        Group catalog = new Group();
        catalog.setString("name", name);
        Group export = catalog.getGroup("export");
        export.setBoolean("xml", exportXml);
        export.setBoolean("startup", exportStartup);
        export.setBoolean("shutdown", exportShutdown);
        long built;
        catalog.setLong("built", built = System.currentTimeMillis());
        out.println("catalog=" + catalog.export());
        String[] keys = new String[articles.size()];
        keys = articles.keySet().toArray(keys);
        for (int i = 0; i < keys.length; i++) {
            Article article = articles.get(keys[i]);
            Group meta = article.getMeta();
            out.println(String.format("%02x=", i) + meta.export());
        }
        out.close();
        return built;
    }

    private void buildXml() {
        File[] files = xmlRoot.listFiles();
        if(files == null) {
            log.warn("Could not build catalog from xml!");
            return;
        }
        for(File file : files) {
            if(file.isFile()) {
                try {
                    Article article = Article.importXml(file, this);
                    articles.put(article.getName(), article);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void build() {
        File map = new File(catalogRoot, "catalog.map");
        if(!map.exists()) {
            log.error("Catalog map not found. Exiting catalog build!");
            return;
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(map)));
            String line;
            while((line = reader.readLine()) != null) {
                Group group = new Group();
                try {
                    String name = ConfigReader.readGroup(line, group);
                    if(name.equals("catalog")) {
                        if(group.has("name") && group.getString("name").equals(this.name)) {
                            built = group.getLong("built");
                            if(group.has("export")) {
                                Group export = group.getGroup("export");
                                exportXml = export.getBoolean("xml");
                                exportShutdown = export.getBoolean("shutdown");
                                exportStartup = export.getBoolean("startup");
                            } else {
                                log.warn("Catalog export values not set. Assuming defaults");
                            }
                        } else {
                            log.error("Read catalog name did not match catalog name. Aborting build!");
                        }
                    } else {
                        Article article;
                        if(group.has("build") && group.getBoolean("build")) {
                            File xml = new File(group.getString("path"));
                            articles.put(group.getString("name"), article = Article.importXml(xml, this));
                        } else {
                            log.info("Loading article " + group.getString("name") + " from config files");
                            File art = new File(catalogRoot, name + ".art");
                            if(!art.exists()) {
                                log.error("Article file not found! Article " + group.getString("name"));
                                continue;
                            }
                            articles.put(group.getString("name"), article = Article.build(art, group.getString("name"), this));
                        }
                        article.setBuild(group.has("build") && group.getBoolean("build"));
                    }
                } catch (Exception e) {
                    log.error("Could not read group!", e);
                }
            }
            if(exportStartup) {
                export();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public boolean isExportXml() {
        return exportXml;
    }

    public boolean isExportStartup() {
        return exportStartup;
    }

    public boolean isExportShutdown() {
        return exportShutdown;
    }
}
