package at.femoweb.config;

import at.femoweb.config.entries.ConfEntry;
import at.femoweb.config.entries.Group;
import at.femoweb.config.entries.List;
import org.apache.commons.lang3.StringEscapeUtils;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class CatalogTest {

    @Test
    public void testConstructor() throws Exception {
        Catalog catalog = Catalog.open("global");
        assertEquals(true, new File("config/global.cat").exists());
        assertEquals(true, new File("config/global").exists());
    }

    @Test
    @Ignore
    public void testOpenArticle() throws Exception {
        Catalog catalog = Catalog.open("global");
        Article article = catalog.openArticle("http.conf");
        assertEquals("http.conf", article.getName());
        Paragraph paragraph = article.openParagraph("socket");
        assertEquals("socket", paragraph.getName());
    }

    @Test
    public void testGroups() throws Exception {
        Paragraph paragraph = Catalog.open("global").openArticle("http.conf").openParagraph("socket");
        Group group = paragraph.openEntry();
        group.setInt("port", 8080);
        group.setBoolean("ssl", false);
        group.setString("hostname", "testserver");
        group.setFloat("max-con-timeout-s", 0.1f);
        assertEquals(8080, group.getInt("port"));
        assertEquals(false, group.getBoolean("ssl"));
        assertEquals("testserver", group.getString("hostname"));
        assertEquals(.1f, group.getFloat("max-con-timeout-s"), 0);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGroupAdd() throws Exception {
        Paragraph paragraph = Catalog.open("global").openArticle("http.conf").openParagraph("socket");
        Group group = paragraph.openEntry();
        group.addString("Holy Moly");
    }

    @Test
    @Ignore
    public void testNestedGroupsAndLists() throws Exception {
        Paragraph paragraph = Catalog.open("global").openArticle("http.conf").openParagraph("socket");
        Group group = paragraph.openEntry();
        group.setInt("port", 8080);
        Group service = group.getGroup("service");
        service.setInt("delay", 2);
        service.setDouble("load-ratio", 0.5);
        assertEquals(0.5, group.getGroup("service").getDouble("load-ratio"), 0);
        assertEquals(2, group.getGroup("service").getInt("delay"));
        List names = group.getList("hostnames");
        names.addString("test_0");
        names.addString("test_1");
        names.addString("test_2");
        assertEquals("test_0", group.getList("hostnames").getString(0));
        System.out.println(group.export());
    }

    @Test
    public void testExport() throws Exception {
        Catalog global = Catalog.open("global");
        Paragraph paragraph = global.openArticle("http.conf").openParagraph("socket");
        Group group = paragraph.openEntry();
        group.setInt("port", 8080);
        group.setBoolean("share", true);
        Group service = group.getGroup("service");
        service.setInt("delay", 2);
        service.setDouble("load-ratio", 0.5);
        List names = group.getList("hostnames");
        names.addString("test_0");
        names.addString("test_1");
        names.addString("test_2");
        Paragraph motd = global.openArticle("server").openParagraph("motd");
        global.openArticle("http.conf").setBuild(false);
        Group message = motd.openEntry();
        message.setString("severity", "warning");
        message.setString("message", "Hello World\nmessage with some\t\bescaped characters");
        global.export();
    }

    @Test
    @Ignore
    public void testCompileXml() throws Exception {
        Catalog catalog = Catalog.open("global", true);
        catalog.export();
    }

    @Test
    public void testBuildXml() throws Exception {
        Catalog catalog = Catalog.open("global", true);
        Paragraph paragraph = catalog.openArticle("http.conf").openParagraph("socket");
        Group group = paragraph.openEntry();
        System.out.println(group.export());
        assertEquals(8080, group.getInt("port"));
        assertEquals(true, group.getBoolean("share"));
    }

    @Test
    public void testReadGroup() throws Exception {
        Group group = new Group();
        String name = ConfigReader.readGroup("motd=$[severity=warning,message=Hello World\\nmessage with some\\t\\bescaped characters]", group);
        assertEquals("motd", name);
        assertEquals("warning", group.getString("severity"));
        assertEquals("Hello World\nmessage with some\t\bescaped characters", group.getString("message"));
    }

    @Test
    @Ignore
    public void testBuild() throws Exception {
        Catalog catalog = Catalog.open("global", false);
        Group group = catalog.openArticle("http.conf").openParagraph("socket").openEntry();
        assertEquals(8080, group.getInt("port"));
        Group motd = catalog.openArticle("server").openParagraph("motd").openEntry();
        System.out.println(motd.export());
        assertEquals("warning", motd.getString("severity"));
        assertEquals("Hello World\nmessage with some\t\bescaped characters", motd.getString("message"));
    }
}